
-- add users to blog_db
INSERT INTO users (email, password, datetime_created) 
	VALUES (
		"johnsmith@gmail.com",
		"passwordA",
		"2021-01-1 01:00:00"
	);
INSERT INTO users (email, password, datetime_created) 
	VALUES (
		"juandelacruz@gmail.com",
		"passwordB",
		"2021-01-1 02:00:00"
	);
INSERT INTO users (email, password, datetime_created) 
	VALUES (
		"janesmith@gmail.com",
		"passwordC",
		"2021-01-1 03:00:00"
	);
INSERT INTO users (email, password, datetime_created) 
	VALUES (
		"mariadelacruz@gmail.com",
		"passwordD",
		"2021-01-1 04:00:00"
	);
INSERT INTO users (email, password, datetime_created) 
	VALUES (
		"johndoe@gmail.com",
		"passwordE",
		"2021-01-1 05:00:00"
	);

-- add post to users
INSERT INTO posts (user_id, title, content, datetime_posted)
	VALUES (
		1,
		"First Code",
		"Hello World",
		"2021-01-02 01:00:00"
	);
INSERT INTO posts (user_id, title, content, datetime_posted)
	VALUES (
		1,
		"Second Code",
		"Hello Earth",
		"2021-01-02 02:00:00"
	);
INSERT INTO posts (user_id, title, content, datetime_posted)
	VALUES (
		2,
		"Third Code",
		"Welcome to Mars",
		"2021-01-02 03:00:00"
	);
INSERT INTO posts (user_id, title, content, datetime_posted)
	VALUES (
		4,
		"Fourth Code",
		"Bye bye Solar System",
		"2021-01-02 04:00:00"
	);

-- Get all post of author id 1
Select posts , user_id FROM posts where user_id = 1;

-- get all users email and datetime
Select email, datetime_created FROM users

-- update post content
UPDATE posts SET content = "HELLO TO THE PEOPLE OF EARTH" WHERE user_id = 2;

-- delete
DELETE FROM users WHERE user_id = 4;